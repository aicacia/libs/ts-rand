# ts-rand

[![license](https://img.shields.io/badge/license-MIT%2FApache--2.0-blue")](LICENSE-MIT)
[![docs](https://img.shields.io/badge/docs-typescript-blue.svg)](https://aicacia.gitlab.io/libs/ts-rand/)
[![npm (scoped)](https://img.shields.io/npm/v/@aicacia/rand)](https://www.npmjs.com/package/@aicacia/rand)
[![pipelines](https://gitlab.com/aicacia/libs/ts-rand/badges/master/pipeline.svg)](https://gitlab.com/aicacia/libs/ts-rand/-/pipelines)

aicacia rand utils
