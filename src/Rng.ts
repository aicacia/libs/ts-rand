import { IIterator, Iterator, Option, some } from "@aicacia/core";
import { MAX_INT } from "./constants";
import { UniformFloatRng } from "./UniformFloatRng";
import { UniformIntRng } from "./UniformIntRng";

const TMP_BYTES = new Uint8Array(4);

export abstract class Rng implements IIterator<number> {
  abstract nextInt(): number;

  nextFloat() {
    return this.nextInt() / MAX_INT;
  }

  nextFloatInRange(min = 0.0, max = 1.0) {
    return this.nextFloat() * (max - min) + min;
  }

  nextIntInRange(min = 0, max = MAX_INT) {
    return Math.round(this.nextFloatInRange(min, max));
  }

  shuffle<T>(array: T[]) {
    const length = array.length;
    for (let i = 0; i < length; i++) {
      const randomIndex = i + this.nextIntInRange(0, length - i - 1),
        tmp = array[i];

      array[i] = array[randomIndex];
      array[randomIndex] = tmp;
    }
    return array;
  }

  fillBytes<B extends Uint8Array | number[] = Uint8Array | number[]>(
    bytes: B
  ): B {
    const tmpBytes = TMP_BYTES;

    for (let i = 0, il = bytes.length; i < il; i++) {
      const index = i % 4;

      if (index === 0) {
        getBytes(tmpBytes, this.nextInt());
      }

      bytes[i] = tmpBytes[index];
    }

    return bytes;
  }

  iter() {
    return new Iterator(this);
  }

  next(): Option<number> {
    return some(this.nextInt());
  }

  uniformFloatRng(min = 0.0, max = 1.0) {
    return new UniformFloatRng(this, min, max);
  }

  uniformIntRng(min = 0, max = MAX_INT) {
    return new UniformIntRng(this, min, max);
  }
}

function getBytes<B extends Uint8Array | number[] = Uint8Array | number[]>(
  bytes: B,
  integer: number
): B {
  bytes[0] = (integer & 0xff000000) >> 24;
  bytes[1] = (integer & 0x00ff0000) >> 16;
  bytes[2] = (integer & 0x0000ff00) >> 8;
  bytes[3] = integer & 0x000000ff;
  return bytes;
}
